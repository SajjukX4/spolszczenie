===[ FDA <=> AIFC converter v1.0 by jTommy

����� ����������
  ��� ���������� ��������� ������������ �������� FDA ����� �� ����
  "W40k: Dawn of War" � "W40k: Winter Assault" � AIFC ����� ����
  "Homeworld II". AIFC ����� ����� ����� ������� � Winamp'�, �
  ������� ������� Relic In_AIFx (http://www.winamp.com/plugins/details.php?id=272)
  ��� ������������ � WAV.

��� ������������
  ��������� ��������� � ������� � FDA �������. ���������� � ���������
  ������: "fda2aifc *.fda". ����� ������� ����� � ����������� .aifc.
  ��� ����� �������� � Winamp ��� ����������� � WAV: "decShell *.aifc".
  � ������� aifc2fda ���������� ����� �������������� AIFC ����� �������
  � FDA ������.

����� � ������
  fda2aifc.exe - ��������� FDA => AIFC.
  aifc2fda.exe - ��������� AIFC => FDA.
  dec.exe - ����������� ������� AIFC => WAV, �� Relic Entertainment Inc.
  enc.exe - ����������� ����� WAV => AIFC, �� Relic Entertainment Inc.
  decShell.exe - ���������� �������� ��� ��������, ������������
                 �������� �����. �������� ��� �������� �������������
                 �������� ���������� ������.
  encShell.exe - �� �� �����, ������ ��� ������.
  fda2aifc_src.zip - ��������� �� ������.

��������
  E-mail: jTommy@zmail.ru
  WWW   : �� �����...
