===[ fda2aifc converter v1.0 by jTommy
===[ ENG
General discription:
  This small program converts FDA files from "Warhammer 40000: Dawn of War"
  game to "Homeworld II" AIFC files. AIFC files can be played using Winamp
  with Relic In_AIFx plugin (http://www.winamp.com/plugins/details.php?id=272).
  Just rename those files extenitions to .aif or .aifc or .aifr. You can also
  convert them to WAV. Delphi sources are included.
How to use:
  First you need to convert DoW FDA files using command "fda2aifc.exe filename.fda"
  or "fda2aifc.exe *.fda" This will create files with .aifc extenition. You can
  already play them with Winamp.
  Now you can decode AIFC files to WAVs: "dec.exe filename.aifc filename.wav".
  Or, if you need to convert many files in one instance use "decShell.exe *.aifc".
  decShell.exe is a console shell for dec.exe, and it supports filename masks.
Files in this archive:
  dec.exe - The official decoder AIFR => WAV, from Relic Entertainment Inc.
  enc.exe - The official coder WAV => AIFR, from Relic Entertainment Inc.
  decShell.exe - The console shell for the decoder, it supports filename masks.
                 (for example *.fda, *.* etc.). If need to convert many files
                 in one instance.
  encShell.exe - Same, only for the coder.
  fda2aifc.exe - Itself converter.
  fda2aifc_src.zip - Delphi sources.
  ReadMe.txt - File, which you read.
Contacts:
  E-mail: jTommy@rambler.ru
  WWW   : I do not remember...

===[ RUS
����� ��������:
  ��� ��������� ��������� ������������ �������� FDA ����� ��  ����
  "Warhammer 40000: Dawn of War" � AIFC ����� ���� "Homeworld II".
  AIFC �����  ����� ����� �������  � Winamp'�, �  �������  �������
  In_AIFx (http://www.winamp.com/plugins/details.php?id=272).
  ��� ������������ � �������� WAV. ��������� �� ������ �����������.
��� ������������:
  ����������,  ��������,  ���: "fda2aifc *.fda",  �����, ����  ���
  �������������  ���  FDA �����  � �������  ����������. ����������
  ����� (� ����������� .aifc) �������� �  Winamp ��� �������������
  � WAV: "decShell *.aifc".
����� � ���� ������:
  dec.exe - ����������� ������� AIFR => WAV, �� Relic Entertainment Inc.
  enc.exe - ����������� ����� WAV => AIFR, �� Relic Entertainment Inc.
  decShell.exe - �������� ��� ��������, ������������ �������� �����.
                 (�������� *.fda, *.* � �.�.). �������� ��� ��������
                 ������������� �������� ���-�� ������.
  encShell.exe - �� �� �����, ������ ��� ������.
  fda2aifc.exe - ��� ���������.
  fda2aifc_src.zip - ��������� �� ������.
  ReadMe.txt - ����, ������� �� �������.
��������:
  E-mail: jTommy@rambler.ru
  WWW   : �� �����...
