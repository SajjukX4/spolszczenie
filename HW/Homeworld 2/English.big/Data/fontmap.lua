-- Mappings from font names to font files for different resolutions for a given language

FontMap640 =
{
    default = "Arial 12",

    --Subtitle and SP game fonts
    SPSubtitleFont = "Arial 12",
    LocationCardFont = "Arial 12",

    --ATI/SM fonts
    ATIControlGroupFont = "Arial 10",
    ATISmallFont = "Arial 9",

    --UI fonts
    Heading1Font = "Arial 22",	-- Title font
    Heading2Font = "Arial 10",	        -- Sub title font
    Heading3Font = "Arial 12",   -- Window title font
    Heading4Font = "Arial 8",			-- Window sub title font
    ButtonFont    = "Arial 9", 			-- Font for buttons
    ListBoxItemFont = "Arial 8",
    ChatFont       = "Arial 8",			-- Font for chat text
    
}

FontMap800 =
{
    default = "Arial 30",

    --Subtitle and SP game fonts
    SPSubtitleFont = "Arial 16",
    LocationCardFont = "Arial 16",

    --ATI/SM fonts
    ATIControlGroupFont = "Arial 13",
    ATISmallFont = "Arial 12",

    --UI fonts
    Heading1Font = "Arial 23",	-- Title font
    Heading2Font = "Arial 13",	        -- Sub title font
    Heading3Font = "Arial 16",   -- Window title font
    Heading4Font = "Arial 11",			-- Window sub title font
    ButtonFont    = "Arial 12", 			-- Font for buttons
    ListBoxItemFont = "Arial 11",
    ChatFont       = "Arial 11",			-- Font for chat text

}

FontMap1024 =
{
    default = "Arial 20",

    --Subtitle and SP game fonts
    SPSubtitleFont = "Arial 20",
    LocationCardFont = "Arial 20",

    --ATI/SM fonts
    ATIControlGroupFont = "Arial 16",
    ATISmallFont = "Arial 14",

    --UI fonts
    Heading1Font = "Arial 31",	-- Title font
    Heading2Font = "Arial 16",	        -- Sub title font
    Heading3Font = "Arial 20",   -- Window title font
    Heading4Font = "Arial 14",			-- Window sub title font
    ButtonFont    = "Arial 15", 			-- Font for buttons
    ListBoxItemFont = "Arial 13",
    ChatFont       = "Arial 13",			-- Font for chat text
    dummyFont = "data:UI\\font\\ArenaBlack.rcf"
}

FontMap1280 =
{
    default = "Arial 23",

    --Subtitle and SP game fonts
    SPSubtitleFont = "Arial 23",
    LocationCardFont = "Arial 23",

    --ATI/SM fonts
    ATIControlGroupFont = "Arial 19",
    ATISmallFont = "Arial 17",

    --UI fonts
    Heading1Font = "Arial 36",	-- Title font
    Heading2Font = "Arial 18",	        -- Sub title font
    Heading3Font = "Arial 23",   -- Window title font
    Heading4Font = "Arial 16",			-- Window sub title font
    ButtonFont    = "Arial 17", 			-- Font for buttons
    ListBoxItemFont = "Arial 16",
    ChatFont       = "Arial 16",			-- Font for chat text
}

FontMap1600 =
{
    default = "Arial 28",

    --Subtitle and SP game fonts
    SPSubtitleFont = "Arial 28",
    LocationCardFont = "Arial 28",

    --ATI/SM fonts
    ATIControlGroupFont = "Arial 23",
    ATISmallFont = "Arial 20",

    --UI fonts
    Heading1Font = "Arial 40",	-- Title font
    Heading2Font = "Arial 22",	        -- Sub title font
    Heading3Font = "Arial 28",   -- Window title font
    Heading4Font = "Arial 19",			-- Window sub title font
    ButtonFont    = "Arial 20", 			-- Font for buttons
    ListBoxItemFont = "Arial 20",
    ChatFont       = "Arial 22",			-- Font for chat text
}

